import { Module } from '@nestjs/common';

import { UsersApiModule } from './users/users.module';

@Module({
  imports: [UsersApiModule],
})
export class ApiModule {}
